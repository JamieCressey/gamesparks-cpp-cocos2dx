# Quick Start Guide for Cocos2d-x

## Prerequisites

### OS X

1. [XCode](https://developer.apple.com/xcode/downloads/)

### Windows

1. [download](https://www.python.org/downloads/) and install python 2.7 (only required on windows)
2. [Visual Studio](http://www.visualstudio.com/downloads/download-visual-studio-vs.aspx)

## running the sample

1. execute install_dependencies.py. This will install a copy of cocos2d-x.
2. Open one of the project file located at GameSparksSample/proj.* in you IDE (note that the Android Studio project does not support building from inside the IDE unless you specify cocos as external build tool as described here: http://discuss.cocos2d-x.org/t/how-can-i-use-android-studio-together-with-cocos2d-x-any-hints/7365)
3. Open Sample/Classes/AppDelegate.h and change the credentials near line 84
4. compile, run

Alternatively you case edit Sample/Classes/AppDelegate.h in any editor and execute build_sample.py

## Integrating the SDK into your project

1. Copy the GameSparksSDK directory into your project
2. Add the source files to your project:
2.1 XCode: Add all the .cpp and .c files located at GameSparksSDK/src/** to your project
2.2 Any other platform: for convenience use GameSparksSDK/src/GameSparksAll.cpp instead of the sources mentioned above.
3. Add GameSparksSDK/include to your include search paths

One thing to note is, that the SDK is not thread-safe. So you have to do locking yourself. However the SDK only ever calls any callbacks from within a call to Update(). So you always know which thread is calling your callbacks.

## Next Steps

Explore the [API docs](http://api.gamesparks.net), the [Doxygen documentation](http://gamesparks.bitbucket.org/cpp-sdk/annotated.html) and the [Examples](http://gamesparks.bitbucket.org/cpp-sdk/examples.html).
